from Listeregle import *
from Renommage import *
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog


class Application(Frame):
    """
    Class Application : Frame
    Basic form

    Base functions : __init__
    Form functions : create_widgets (init all forms/labels/texts/buttons) ; rule_form (rule manager form)
    Message fucntions : show_rules (info to show all rules) ; show_about(info to show all soft info)
    Content functions : browse (select the folder)
    App functions : create (add new rule) ; rename(rename files)
    Other fucntions : name_file / original_file (activate/desactivate the entry for file name)
    """

    global version
    version = "1.0.5"

    global rules
    rules = Listeregle()
    rules.charger("RENAMING.ini")

    def __init__(self, master=None):
        """
        Constructor
        :param master: None
        Use create_widgets()
        """
        Frame.__init__(self, master)
        self.pack()

    def rule_form(self):
            """
            Change the current form to manage the rules
            :return: void
            """
            #       BUTTONS
            self.B_RENAME.grid_remove()
            self.B_BROWSE.grid_remove()
            #       LABELS
            self.L_NAME_RENAMING.grid_remove()
            self.L_NAME_DIRECTORY.grid_remove()
            self.L_NAME_RULE.grid_remove()
            #       ENTRY
            self.E_DIRECTORY.grid_remove()
            #       LISTBOX
            self.LB_RULES.grid_remove()

            #       BUTTONS
            self.B_CREATE.grid()
            self.B_BACK.grid()
            #       LABELS
            self.L_NAME_RULES.grid()
            self.L_NAME_RULE_R.grid()
            self.L_AMORCE.grid()
            self.L_APARTIRDE.grid()
            self.L_PREFIXE.grid()
            self.L_POSTFIXE.grid()
            self.L_EXTENSIONS.grid()
            #       ENTRIES
            self.E_RULE.grid()
            self.E_APARTIRDE.grid()
            self.E_PREFIXE.grid()
            self.E_NOMORIGINAL.grid()
            self.E_POSTFIXE.grid()
            self.E_EXTENSIONS.grid()
            #       RADIO BUTTONS
            self.RB_NOMFICHIER.grid()
            self.RB_NOMORIGINAL.grid()
            self.RB_AMORCE_NONE.grid()
            self.RB_AMORCE_LETTER.grid()
            self.RB_AMORCE_DIGIT.grid()

    def load_renaming(self):
            """
            Change the current form to the renaming manager
            :return: void
            """
            #       BUTTONS
            self.B_CREATE.grid_remove()
            self.B_BACK.grid_remove()
            #       LABELS
            self.L_NAME_RULES.grid_remove()
            self.L_NAME_RULE_R.grid_remove()
            self.L_AMORCE.grid_remove()
            self.L_APARTIRDE.grid_remove()
            self.L_PREFIXE.grid_remove()
            self.L_POSTFIXE.grid_remove()
            self.L_EXTENSIONS.grid_remove()
            #       ENTRIES
            self.E_RULE.grid_remove()
            self.E_APARTIRDE.grid_remove()
            self.E_PREFIXE.grid_remove()
            self.E_NOMORIGINAL.grid_remove()
            self.E_POSTFIXE.grid_remove()
            self.E_EXTENSIONS.grid_remove()
            #       RADIO BUTTONS
            self.RB_NOMFICHIER.grid_remove()
            self.RB_NOMORIGINAL.grid_remove()
            self.RB_AMORCE_NONE.grid_remove()
            self.RB_AMORCE_LETTER.grid_remove()
            self.RB_AMORCE_DIGIT.grid_remove()

            #       BUTTONS
            self.B_RENAME.grid()
            self.B_BROWSE.grid()
            #       LABELS
            self.L_NAME_RENAMING.grid()
            self.L_NAME_DIRECTORY.grid()
            self.L_NAME_RULE.grid()
            #       ENTRY
            self.E_DIRECTORY.grid()
            #       LISTBOX
            self.LB_RULES.grid()

    def show_rules(self):
            """
            Show all rules in a new window
            :return: void
            """
            j = 0
            message = ""
            rule_list = ["Nom règle | Amorce | A partir de | Préfixe | Nom fichier | Suffixe | Extensions\n\n"]

            #   Loop through the rules
            for rule in rules.regles:
                chain = ""
                j += 1
                chain += rule.nom + " | " + rule.amorce + " | " + rule.apartirde + " | " + rule.prefixe + " | "
                chain += rule.nom_fichier + " | " + rule.postfixe + " | ["

                #   Loop through the extensions
                for e in rule.extension:
                    chain += e + " , "

                chain += "]\n"

                #   Add all components
                rule_list.append(chain)

            #   Loop through the list
            for str in rule_list:
                message += str + "\n"

            #   Display all
            messagebox.showinfo("Rules list", message)

    def show_about(self):
            """
            Show program version in a new window
            :return void
            """
            #   Display all
            messagebox.showinfo("About", "Version : " + version + "\n Auteur : Antoine BASLÉ")

    def name_file(self):
        """
        Enable the entry for file name if RB original is selected ; disable it else
        :return: void
        """
        self.E_NOMORIGINAL["state"] = DISABLED

    def original_file(self):
        """
        Enable the entry for file name if RB original is selected ; disable it else
        :return: void
        """
        self.E_NOMORIGINAL["state"] = NORMAL

    def browse(self):
        """
        Let the user chose the path
        :return: void
        """
        self.E_DIRECTORY.insert(0, filedialog.askdirectory())

    def leave(self):
        """
        Destroy all sessions
        :return: void
        """
        self.quit()
        app.quit()
        root.quit()

    def create_widgets(self):
        """
        Create all buttons/texts/labels
        :return: void
        """
#       RENAME PART
#       Buttons
        self.B_RENAME = Button(self)
        self.B_RENAME["text"] = "Renommer"
        self.B_RENAME["command"] = self.rename
        self.B_RENAME.grid(row=10, column=6)

        self.B_BROWSE = Button(self, width=3)
        self.B_BROWSE["text"] = "..."
        self.B_BROWSE["command"] = self.browse
        self.B_BROWSE.grid(row=3, column=5)

#       Labels
        self.L_VOID = Label(self)
        self.L_VOID.grid(row=3, column=5)

        self.L_NAME_RENAMING = Label(self, text="Renommer en lots")
        self.L_NAME_RENAMING.grid(row=2, column=4)

        self.L_NAME_DIRECTORY = Label(self, text="Nom du répertoire")
        self.L_NAME_DIRECTORY.grid(row=3, column=3)

        self.L_NAME_RULE = Label(self, text="Règle")
        self.L_NAME_RULE.grid(row=5, column=3)

#       Entry
        self.E_DIRECTORY = Entry(self, width=50)
        self.E_DIRECTORY.grid(row=3, column=4)

#       Listbox
        self.LB_RULES = Listbox(self, selectmode=SINGLE, height=3, width=50)
        self.LB_RULES.grid(row=5, column=4)
        self.LB_RULES.yview()

        i = 0

        for regle in rules.regles:
            i += 1
            self.LB_RULES.insert(i, regle.nom)

#       RULE PART
#       Buttons
        self.B_CREATE = Button(self, text='Créer', command=self.create)
        self.B_CREATE.grid(row=10, column=6)
        self.B_CREATE.grid_remove()

        self.B_BACK = Button(self, text="Retour", command=self.load_renaming)
        self.B_BACK.grid(row=12, column=8)
        self.B_BACK.grid_remove()

#       Labels
        self.L = Label(self, text="")
        self.L.grid(row=4, column=1)

        self.L_NAME_RULES = Label(self, text="Créer une règle")
        self.L_NAME_RULES.grid(row=2, column=4)
        self.L_NAME_RULES.grid_remove()

        self.L_NAME_RULE_R = Label(self, text="Nom de la règle")
        self.L_NAME_RULE_R.grid(row=3, column=3)
        self.L_NAME_RULE_R.grid_remove()

        self.L_AMORCE = Label(self, text="Amorce")
        self.L_AMORCE.grid(row=5, column=1)
        self.L_AMORCE.grid_remove()

        self.L_APARTIRDE = Label(self, text="A partir de")
        self.L_APARTIRDE.grid(row=5, column=2)
        self.L_APARTIRDE.grid_remove()

        self.L_PREFIXE = Label(self, text="Préfixe")
        self.L_PREFIXE.grid(row=5, column=3)
        self.L_PREFIXE.grid_remove()

        self.L_POSTFIXE = Label(self, text="Suffixe")
        self.L_POSTFIXE.grid(row=5, column=5)
        self.L_POSTFIXE.grid_remove()

        self.L_EXTENSIONS = Label(self, text="Extensions concernées")
        self.L_EXTENSIONS.grid(row=5, column=6)
        self.L_EXTENSIONS.grid_remove()

#       Entries
        self.E_RULE = Entry(self, width=50)
        self.E_RULE.grid(row=3, column=4)
        self.E_RULE.grid_remove()

        self.E_APARTIRDE = Entry(self, width=5)
        self.E_APARTIRDE.grid(row=6, column=2)
        self.E_APARTIRDE.grid_remove()

        self.E_PREFIXE = Entry(self, width=10)
        self.E_PREFIXE.grid(row=6, column=3)
        self.E_PREFIXE.grid_remove()

        self.E_NOMORIGINAL = Entry(self, width=20)
        self.E_NOMORIGINAL.grid(row=9, column=4)
        self.E_NOMORIGINAL.grid_remove()

        self.E_POSTFIXE = Entry(self, width=10)
        self.E_POSTFIXE.grid(row=6, column=5)
        self.E_POSTFIXE.grid_remove()

        self.E_EXTENSIONS = Entry(self, width=20)
        self.E_EXTENSIONS.grid(row=6, column=6)
        self.E_EXTENSIONS.insert(0, ".ext, .ext")
        self.E_EXTENSIONS.grid_remove()

#       Radio buttons
        global var
        var = IntVar()
        global var2
        var2 = IntVar()

        self.RB_NOMFICHIER = Radiobutton(self, text="Nom du fichier de base", variable=var, value=1, command=self.name_file)
        self.RB_NOMFICHIER.select()
        self.RB_NOMFICHIER.grid(row=6, column=4)
        self.RB_NOMFICHIER.grid_remove()

        self.RB_NOMORIGINAL = Radiobutton(self, text="Nom original", variable=var, value=2, command=self.original_file)
        self.RB_NOMORIGINAL.grid(row=8, column=4)
        self.RB_NOMORIGINAL.grid_remove()

        self.RB_AMORCE_NONE = Radiobutton(self, text="Aucune", variable=var2, value=1, indicatoron=0)
        self.RB_AMORCE_NONE.select()
        self.RB_AMORCE_NONE.grid(row=6, column=1)
        self.RB_AMORCE_NONE.grid_remove()

        self.RB_AMORCE_LETTER = Radiobutton(self, text="Lettre", variable=var2, value=2, indicatoron=0)
        self.RB_AMORCE_LETTER.grid(row=7, column=1)
        self.RB_AMORCE_LETTER.grid_remove()

        self.RB_AMORCE_DIGIT = Radiobutton(self, text="Chiffre", variable=var2, value=3, indicatoron=0)
        self.RB_AMORCE_DIGIT.grid(row=8, column=1)
        self.RB_AMORCE_DIGIT.grid_remove()

#       Menu buttons
        self.MB_MENUBAR = Menu(self)

        self.M_REGLES = Menu(self.MB_MENUBAR, tearoff=0)
        self.M_REGLES.add_command(label="Lister", command=self.show_rules)
        self.M_REGLES.add_command(label="Créer", command=self.rule_form)
        self.M_REGLES.add_separator()
        self.M_REGLES.add_command(label="Quitter", command=self.leave)

        self.M_ABOUT = Menu(self.MB_MENUBAR, tearoff=0)
        self.M_ABOUT.add_command(label="A propos", command=self.show_about)

        self.MB_MENUBAR.add_cascade(label="Règles ↓", menu=self.M_REGLES)
        self.MB_MENUBAR.add_cascade(label="?", menu=self.M_ABOUT)
#       Image
        self.P_IMAGE = PhotoImage(file="a.gif")
        self.P_LABEL = Label(self, image=self.P_IMAGE)
        self.P_LABEL.grid(row=3, column=6)

    def create(self):
        """
        Create a new rule
        :return: void
        """
        #   Checking
        clear = 0

        #   Rule name
        if self.E_RULE.get() != "":
            clear += 1
        else:
            messagebox.showwarning("Attention", "Le nom de la règle n'est pas renseigné")

        #   A partir de (<= 3)
        if self.E_APARTIRDE.get() != "" and len(self.E_APARTIRDE.get()) <= 3:
            clear += 1
        else:
            if self.E_APARTIRDE.get() == "":
                messagebox.showinfo("Attention", "Le champ \"à partir de\" n'est pas renseigné, valeur de base à A ou 001")
            else:
                self.E_APARTIRDE.delete(0, END)
                messagebox.showwarning("Attention", "La valeur \"à partir de\" est trop grande")

        #   Prefixe
        if self.E_PREFIXE.get() != "":
            clear += 1

        #   File name
        if self.E_NOMORIGINAL.get() != "" and var.get() == 2:
            clear += 1

        #   Postfixe
        if self.E_POSTFIXE.get() != "":
            clear += 1

        #   Extensions
        if self.E_EXTENSIONS != "":
            #   Correct writing
            if self.E_EXTENSIONS.get().find('..') < 0:
                clear += 1
            else:
                clear = 0
                messagebox.showwarning("Attention", "Une extension est mal renseignée")

        if clear >= 2:
            #   Get from entries
            rule_apartirde, rule_prefix, rule_postfix, rule_file = "", "", "", "-"
            rule_name = self.E_RULE.get()
            rule_prefix = self.E_PREFIXE.get()
            rule_postfix = self.E_POSTFIXE.get()
            rule_extensions = self.E_EXTENSIONS.get().replace(" ", "").split(",")

            #   Amorce
            if var2.get() == 2:
                rule_amorce = "A"
            elif var2.get() == 3:
                rule_amorce = "001"
            else:
                rule_amorce = ""

            if self.E_APARTIRDE.get() != "" and var2.get() != 1:
                rule_apartirde = self.E_APARTIRDE.get()
            elif var2.get() == 2:
                rule_apartirde = "A"
            else:
                rule_apartirde = "001"

            #   File name
            if self.E_NOMORIGINAL["state"] != DISABLED:
                rule_file += self.E_NOMORIGINAL.get()

            try:
                #   Create the new born
                new_rule = Regle(str(rule_name), str(rule_amorce), str(rule_apartirde), str(rule_prefix), str(rule_file), str(rule_postfix), list(rule_extensions))
            except:
                messagebox.showerror("Erreur", "Une erreur est survenue lors de la création de la règle")

            try:
                #   Add the new rule to the list
                rules.regles.append(new_rule)
                rules.sauvegarder("RENAMING.ini")

                #   Message box
                messagebox.showinfo("Done", "A new rule has been added")

                #   List box
                self.LB_RULES.insert(self.LB_RULES.size(), new_rule.nom)
            except:
                messagebox.showerror("Erreur", "Une erreur est survenue lors de l'ajout de la règle")

        else:
            messagebox.showwarning("Attention", "Un champs n'est pas renseigné")

    def rename(self):
        """
        Rename everything
        :return: void
        """
        #   Get entries
        dire = self.E_DIRECTORY.get()
        rul = Regle()

        #   Check dir is set
        if self.E_DIRECTORY.get() != "" and os.path.exists(dire):
            #   Check if rule is set
            if self.LB_RULES.curselection() != ():
                rul = rules.regles[int(str(self.LB_RULES.curselection())[1])]

                #   Renaming
                rename = Renommage(dire, rul)
                val = rename.renommer()

                messagebox.showinfo("Renommage", str(val) + " fichiers renommés !")
            else:
                messagebox.showwarning("Attention", "Aucune règle n'est sélectionnée")
        else:
            messagebox.showwarning("Attention", "Aucun nom de répertoire n'est renseigné ou celui-ci est incorrecte")


#   Init
root = Tk()
app = Application(master=root)
app.create_widgets()
root.config(menu=app.MB_MENUBAR)
root.title("Renommer")

#   Center window
root.update_idletasks()
w = root.winfo_screenwidth()
h = root.winfo_screenheight()
size = tuple(int(_) for _ in root.geometry().split('+')[0].split('x'))
x = w/2 - size[0]/2 - 150
y = h/2 - size[1]/2 - 50
root.geometry("%dx%d+%d+%d" % ((size[0] + 300, size[1] + 100) + (x, y)))

#   Loop
app.mainloop()
app.quit()
root.quit()
