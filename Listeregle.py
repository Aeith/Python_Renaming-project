from Regle import *


class Listeregle:
    """
    Class Listeregle

    Base functions : __init__ ; __str__
    Getters : _get_regles
    Setters : _set_regles
    Other : charger() ; sauvegarder()
    """

    def __init__(self, regles=[]):
        """
        Constructor
        :param regles: list
        """
        self.regles = regles

    def _get_regles(self):
        """
        Getter
        :return: list
        """
        return self.regles

    def _set_regles(self, value):
        """
        Setter
        :param value: list
        :return: void
        """
        for v in value:
            self.regles.append(v)

    def charger(self, file):
        """
        Load file rules
        :param file: str file name
        :return: void
        """
        nrule = []

        fichier = open(file, 'r')
        rules = str(fichier.read()).split('\n')

        for r in rules:
            if r != "":
                rule = r.split(';')

                regle = Regle(rule[0], rule[1], rule[2], rule[3], rule[4], rule[5], str(rule[6]).replace(' ', '').split(','))
                nrule.append(regle)

        fichier.close()

        self._set_regles(nrule)

    def sauvegarder(self, file):
        """
        Save rules in file
        :param file: str file name
        :return: void
        """
        fichier = open(file, 'w')

        for rule in self.regles:
            fichier.write(rule.__str__() + "\n")

        fichier.close()

    def __str__(self):
        """
        To string
        :return: str
        """
        chaine = "\n "

        for regle in self.regles:
            chaine += (regle.__str__())

        chaine += "\n\n"

        return chaine
