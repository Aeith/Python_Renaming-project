from Action import Action
from Regle import Regle
import os
from os.path import isfile, join


class Renommage(Action):
    """
    Class Renommage

    Base functions : __init__ ; __str__
    Getters : _get_nom_repertoire ; _get_regle
    Setters : _set_nom_repertoire ; _set_regle
    Other : renommer()
    """

    def __init__(self, nom_repertoire="", regle=Regle()):
        """
        Cosntructor
        :param nom_repertoire: str
        :param regle: Regle()
        """
        Action.__init__(self, nom_repertoire, regle)

    def _get_nom_repertoire(self):
        """
        Getter
        :return: str
        """
        return Action._get_nom_repertoire(self)

    def _get_regle(self):
        """
        Getter
        :return: Regle()
        """
        return Action._get_regle(self)

    def _set_nom_repertoire(self, value):
        """
        Setter
        :param value: str
        :return: void
        """
        try:
            Action._set_nom_repertoire(self, value)
        except ValueError:
            print("Erreur : type de valeur incorrecte ; chaine attendue")

    def _set_regle(self, value):
        """
        Setter
        :param value: Regle()
        :return: void
        """
        try:
            Action._set_regle(self, value)
        except ValueError:
            print("Erreur : type de valeur incorrecte ; Regle attendue")

    def renommer(self):
        """
        Rename all files in the dir
        :return: void
        """
        count = 0
        files = []

        try:
            files = [f for f in os.listdir(self._get_nom_repertoire()) if isfile(join(self._get_nom_repertoire(), f))]
        except IOError:
            print("Erreur : dossier introuvable")

        for file in files:
            count += 1

            """
            Print the simulated name the file will have after the renaming
            :return: void
            """
            original = ""
            extension = ""
            changed = ""

            try:

                #   Get original name
                #   Correct ext
                if ('.' + file.split('.')[1] in self.regle.extension) or self.regle.extension == ['']:

                    if self.regle.nom_fichier != "-":
                        original = self.regle.nom_fichier
                    else:
                        original = file.split('.')[0]

                    extension = '.' + file.split('.')[1].lower()
                    print("Nom du fichier original : " + file)

                #           Amorce
                if self.regle.amorce != "":
                    #   Digits
                    if self.regle.amorce.isnumeric():
                        # More 3 digits : Letters
                        if int(self.regle.apartirde) == 999:
                            changed += "A" + original
                        # Less : add 1
                        else:
                            changed += '{:03}'.format((int(self.regle.apartirde) + 1)) + original
                    #   Alpha
                    else:
                        name = original
                        flagDigit = False
                        flagEnd = False
                        length = len(self.regle.apartirde)
                        amorce = self.regle.apartirde.upper()
                        liste = os.listdir(self.nom_repertoire)

                        #   For each letter in the apd
                        for i in range(3):

                            #   If hasn't been renamed
                            if not flagEnd:

                                #   Current letter = current amorce letter
                                if i == 0:
                                    letter = amorce[-i - 1]
                                else:
                                    letter = amorce[-i]

                                #   Not end of letters
                                if amorce != "ZZZ":

                                    #   While not renamed OR letter next step in letters
                                    while True:
                                        temp = amorce + self.regle.prefixe.lower() + original + self.regle.postfixe.lower() + extension

                                        #   If exists
                                        if temp in liste:

                                            #   If letter isn't Z, letter + 1
                                            if letter != "Z":
                                                letter = chr(int(ord(letter)) + 1)
                                                aliste = list(amorce)

                                                #   Reversing
                                                if i == 0:
                                                    aliste[-i - 1] = letter
                                                else:
                                                    aliste[-i] = letter

                                                amorce = ""

                                                for a in aliste:
                                                    amorce += a

                                            #   If letter is Z, next step letter
                                            else:

                                                #   3 letters
                                                if len(amorce) == 3:
                                                    if amorce[2] == "Z":
                                                        if amorce[1] == "Z":
                                                            amorce = chr(int(ord(amorce[0]) + 1)) + "AA"
                                                        else:
                                                            amorce = amorce[0] + chr(int(ord(amorce[1]) + 1)) + "A"
                                                    else:
                                                        amorce = amorce[0:1] + chr(int(ord(amorce[2]) + 1)) + "A"

                                                #   2 letters
                                                elif len(amorce) == 2:
                                                    if amorce[1] == "Z":
                                                        if amorce == "ZZ":
                                                            amorce = "AAA"
                                                        else:
                                                            amorce = chr(int(ord(amorce[0]) + 1)) + "A"
                                                    else:
                                                        amorce = amorce[0] + chr(int(ord(amorce[1]) + 1))

                                                #   1 letter
                                                else:
                                                    if amorce == "Z":
                                                        amorce = "AA"
                                                    else:
                                                        amorce = chr(int(ord(amorce) + 1))

                                                break

                                        #   If not exists
                                        else:
                                            flagEnd = True
                                            break

                                # End of letters : we switch to digits
                                else:
                                    changed = "001"
                                    flagDigit = True
                                    break

                            else:
                                break

                        if not flagDigit:
                            changed = amorce.upper()

                #           Prefix
                if self.regle.prefixe != "":
                    changed += self.regle.prefixe.lower() + original
                else:
                    changed += original

                #           Postfix
                if self.regle.postfixe.lower() != "":
                    changed += self.regle.postfixe.lower() + extension
                else:
                    changed += extension

            except:
                print("Erreur : code error")

            try:
                original = os.path.join(self._get_nom_repertoire(), original + extension)
                changed = os.path.join(self._get_nom_repertoire(), changed)
            except IOError:
                print("Erreur : chemin introuvable")

            try:
                os.rename(original, changed)
                print("Nom du fichier post-renommage : " + changed)
            except IOError:
                print("Erreur : renommage impossible")

        return count

    def __str__(self):
        """
        To string
        :return: string
        """
        return Action.__str__(self)
