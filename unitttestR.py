import unittest
from Renommage import *


class TestRenommage(unittest.TestCase):

    def test_renommer(self):
        rename_test = Renommage("Test", Regle("A", "A", "Z", "Pré", "", "Post", ".txt"))
        self.assertEqual(rename_test.renommer(), 9)


if __name__ == '__main__':
    unittest.main()
