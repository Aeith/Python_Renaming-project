class Regle:
    """
    Class regle

    Base functions : __init__ ; __str__
    Getters : _get_amorce ; _get_apartirde ; _get_prefixe ; _get_nom_fichier ; _get_postfixe ; _get_extension
    Setters : _set_amorce ; _set_apartirde ; _set_prefixe ; _set_nom_fichier ; _set_postfixe ; _set_extension
    Other : None
    """

    def __init__(self, nom="", amorce='', apartirde="", prefixe="", nom_fichier="False", postfixe="", extension=[]):
        """
        Constructor
        :param amorce: null, int, str
        :param apartirde: str
        :param prefixe: str
        :param nom_fichier: str file name
        :param postfixe: str
        :param extension: list
        """
        self.nom = nom
        self.amorce = amorce
        self.apartirde = apartirde
        self.prefixe = prefixe
        self.nom_fichier = nom_fichier
        self.postfixe = postfixe
        self.extension = extension

    def _get_nom(self):
        """
        Getter
        :return: str
        """
        return self.nom

    def _get_amorce(self):
        """
        Getter
        :return: str
        """
        return self.amorce

    def _get_apartirde(self):
        """
        Getter
        :return: str
        """
        return self.apartirde

    def _get_prefixe(self):
        """
        Getter
        :return: str
        """
        return self.prefixe

    def _get_nom_fichier(self):
        """
        Getter
        :return: str file name
        """
        return self.nom_fichier

    def _get_postfixe(self):
        """
        Getter
        :return: str
        """
        return self.postfixe

    def _get_extension(self):
        """
        Getter
        :return: extension
        """
        return self.extension

    def _set_nom(self, value):
        """
        Setter
        :return: void
        """
        self.nom = value

    def _set_amorce(self, value):
        """
        Setter
        :param value: str
        :return: void
        """
        try:
            self.amorce = value
        except ValueError:
            print("Erreur : type de valeur incorrecte ; chaine attendue")

    def _set_apartirde(self, value):
        """
        Setter
        :param value: str
        :return: void
        """
        try:
            self.apartirde = value
        except ValueError:
            print("Erreur : type de valeur incorrecte ; chaine attendue")

    def _set_prefixe(self, value):
        """
        Setter
        :param value: str
        :return: void
        """
        try:
            self.prefixe = value
        except ValueError:
            print("Erreur : type de valeur incorrecte ; chaine attendue")

    def _set_nom_fichier(self, value):
        """
        Setter
        :param value: str
        :return: void
        """
        try:
            self.nom_fichier = value
        except ValueError:
            print("Erreur : type de valeur incorrecte ; chaine attendue")

    def _set_postfixe(self, value):
        """
        Setter
        :param value: str
        :return: void
        """
        try:
            self.postfixe = value
        except ValueError:
            print("Erreur : type de valeur incorrecte ; chaine attendue")

    def _set_extension(self, value):
        """
        Setter
        :param value: list
        :return: void
        """
        try:
            self.extension = value
        except ValueError:
            print("Erreur : type de valeur incorrecte ; chaine attendue")

    def __str__(self):
        """
        To string
        :return: string
        """
        extensions = ""

        #   Proper ext writing
        for i in range(len(self.extension)):
            if i < len(self.extension) - 1:
                extensions += self.extension[i] + ","
            else:
                extensions += self.extension[i]

        chaine = str(self.nom) + ";" + (str(self.amorce) + ";" + str(self.apartirde) + ";")
        chaine += str(self.prefixe) + ";" + str(self.nom_fichier) + ";"
        return chaine + str(self.postfixe) + ";" + extensions
